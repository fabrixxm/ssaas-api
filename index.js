const createError = require('micro').createError;
const json = require('micro').json;
const url = require('url');
const handler = require('serve-handler');
const fetch = require('node-fetch');

const Store = {
    _key_name : 'ssaas',

    _new_id: async () => {
        const request = await fetch('https://api.keyvalue.xyz/new/' + Store._key_name, { method: 'POST' });
        const res = await request.text();
        const id = res.replace('https://api.keyvalue.xyz/', '').replace('/' + Store._key_name, '').replace('\n', '');
        //console.log('Store new id', id);
        return id;
    },

    new: async (participants) => {
        const id = await Store._new_id();

        const winner = participants[Math.floor(Math.random() * participants.length)];
        //console.log('and the winner is', winner);

        var draw = {
            token: id,
            result: participants.map((p) => {
                var l = 'long';
                if (p === winner) l = 'short';
                //console.log('p', p, 'l', l);
                return {
                    'name': p, 
                    'straw': l
                };
            })
        }

        await Store.set(id, draw);

        return draw;
    },

    get: async (id) => {
        const request = await fetch('https://api.keyvalue.xyz/' + id + '/' + Store._key_name);
        var value = await request.json();
        return value;
    },

    set: async (id, value) => {
        body = JSON.stringify(value);
        const request = await fetch('https://api.keyvalue.xyz/' + id + '/' + Store._key_name, { method: 'POST', body: body });
        const res = await request.text();
        return value;
    }

}

async function api(parts, req, res) {
    if (parts[1] === 'draw') {
        if (req.method === 'POST') {
            const participants = await json(req);
            return await Store.new(participants);
        }
        if (req.method === 'GET') {
            return await Store.get(parts[2]);
        }
        throw createError(405, 'Method Not Allowed');
    }

    throw createError(404, 'Not Found');
}

module.exports = async (req, res) => {
    const parsed = url.parse(req.url);
    const parts = parsed.path.match(/^\/api\/v1\/([a-z]+)\/?([a-z0-9]*)/);

    if (parts) {
        return await api(parts, req, res);
    } else {
        await handler(req, res, {
            'public': 'public'
        });
    }
}
