# Short Straw as a Service

Send a request with participants and see who draws the short straw!


## Install

    $ npm install
    $ npm run build
    $ npm run start

## develop

Build swagger docs

    $ npm run build

Start server in dev mode

    $ npm run dev


